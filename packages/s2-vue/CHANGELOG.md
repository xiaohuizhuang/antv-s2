# [@antv/s2-vue-v1.0.1](https://github.com/antvis/S2/compare/@antv/s2-vue-v1.0.0...@antv/s2-vue-v1.0.1) (2022-06-02)


### Bug Fixes

* **locale:** 修复国际化配置不生效 close [#1394](https://github.com/antvis/S2/issues/1394) ([#1397](https://github.com/antvis/S2/issues/1397)) ([cfd5dbe](https://github.com/antvis/S2/commit/cfd5dbe0344afbb6f3929bece1778c02f9bbc00b))
* **vue:** 统一表组件变量名保持和 react 版本一致 Sheet => SheetComponent ([#1389](https://github.com/antvis/S2/issues/1389)) ([0bee476](https://github.com/antvis/S2/commit/0bee4767ecfbe9b87c71f4f52b5569f13bb58686))

# @antv/s2-vue-v1.0.0 (2022-05-30)


### Features

* Vue 1.0 ([#1290](https://github.com/antvis/S2/issues/1290)) ([0745836](https://github.com/antvis/S2/commit/07458368d7eafd3ddee168d5b2adca463374ab5a))


### Reverts

* Revert "chore(release): @antv/s2@1.18.0 @antv/s2-react@1.16.0 @antv/s2-vue@1.0.0 (#1384)" (#1386) ([60cdf6a](https://github.com/antvis/S2/commit/60cdf6abe7fb3b44f831051bd55622587a0f5bf8)), closes [#1384](https://github.com/antvis/S2/issues/1384) [#1386](https://github.com/antvis/S2/issues/1386)
